import React, { Component } from 'react';
import SplashScreen from './screen/SplashScreen';
import Login from './screen/Login';
import HomeScreen from './screen/HomeScreen';
import ChatScreen from './screen/ChatScreen';
import ProfileScreen from './screen/ProfileScreen';
import AuthLoadingScreen from './screen/AuthLoadingScreen'
import { createAppContainer, createSwitchNavigator } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import { createBottomTabNavigator, BottomTabBar } from 'react-navigation-tabs';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';


const AppStack = createStackNavigator (
  {
    Home: HomeScreen,
    Chat: ChatScreen,
    
  });




const TabScreens = createBottomTabNavigator(
    {
      Home: AppStack,
      Profile: ProfileScreen,
    },
    {
      // initialRouteName: 'Home',
      defaultNavigationOptions: ({navigation}) => ({
        tabBarIcon:({focused, tintColor}) => {
          const {routeName} = navigation.state;
          let iconName = null;
          if (routeName === 'Home'){
            iconName = `home${focused ? '' : '-outline'}`;
          } else if (routeName === 'Profile'){
            iconName = `account${focused ? '' : '-outline'}`;
          }
    
          return <Icon name={iconName} size={24} color={tintColor} />;
        },
      }),
      tabBarComponent: BottomTabBar,
      tabBarPosition: 'bottom',
      tabBarOptions: {
        activeTintColor: '#007AFF',
        inactiveTintColor: '#A7A7A7',
        showLabel: true,
        style: {
          elevation: 4,
          height: 60,
        },
      },
    },
);

const AuthStack = createStackNavigator ({ Login: Login });

export default createAppContainer(createSwitchNavigator(
  { 
    Splash: SplashScreen,
    Bottom: TabScreens,
    AuthLoading: AuthLoadingScreen,
    // App: AppStack,
    Auth: AuthStack,
    
  },
  {
    initialRouteName: 'Splash',
  }
));


