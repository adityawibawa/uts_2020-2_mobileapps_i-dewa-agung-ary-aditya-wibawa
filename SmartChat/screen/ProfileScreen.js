import React from 'react'
import { Alert, AsyncStorage, SafeAreaView, Text, TextInput, TouchableOpacity, Image } from 'react-native'
import User from'../User'
import styles from '../constants/styles'
import firebase from 'firebase'

export default class ProfileScreen extends React.Component{
    static navigationOptions = {
        title : 'Profile'
    }

    state = {
        name: User.name
    }

    handleChange = key => val => {
        this.ListeningStateChangedEvent({[key]:val})
    }

    _logOut = async() => {
        await AsyncStorage.clear()
        this.props.navigation.navigate('Auth')
    }

    changeName = async () => {
        if (this.state.name.length <3){
            Alert.alert('Error', 'Please give valid name')
        } else if(User.name !== this.state.name){
            firebase.database().ref('users').child(User.phone).set({name:this.state.name})
            User.name = this.state.name
            Alert.alert('Success', 'Name Change Succesfully')
        }
    }

    render(){
        return(
            <SafeAreaView style={styles.container}>
                <Image
                    style={{height: 100, width: 100}}
                    source={require('../img/logo.png')}
                />
                <Text style={{fontSize:20, fontWeight: 'bold', marginBottom: 10, marginTop: 10}}>{User.name}</Text>
                <Text style={{fontSize:15}}>{User.phone}</Text>

                <TouchableOpacity onPress={this._logOut} style={{marginTop: 20}}>
                    <Text style={styles.btnText}>Logout</Text>
                </TouchableOpacity>
            </SafeAreaView>
        )
    }

}