import React from 'react'
import { View, Dimensions, FlatList, SafeAreaView, Text, TextInput, TouchableOpacity, Image } from 'react-native'
import User from'../User'
import styles from '../constants/styles'
import firebase from 'firebase'
import Icon from 'react-native-vector-icons/FontAwesome'
import ChatBot from 'react-native-chatbot';
import { UserImg } from '../constants/MessagesStyles'


const sendIcon = <Icon name="arrow-up" size={30} color="#000" />
const fileIcon = <Icon name="paperclip" size={30} color="#000" />
export default class ChatScreen extends React.Component {
    static navigationOptions = ({navigation}) => {
        return {
            tabBarVisible: false,
            title:navigation.getParam('name', null),
            headerTitleStyle: { alignSelf: 'center' },
            headerRight: (
                <TouchableOpacity onPress={() => navigation.navigate('Profile')} style={{marginRight: 10}}>
                    <Image
                        style={{height: 30, width: 30}}
                        source={require('../img/logo.png')}
                    />
                </TouchableOpacity>
            ),
            
        }
    }

    constructor(props){
        super(props)
        this.state = {
            person: {
                name : props.navigation.getParam('name'),
                phone : props.navigation.getParam('phone'),
            },
            data: [],
            mappedData: [],
            textMessage: '',
            messageList: [],
            steps: [],
        }
    }

    UNSAFE_componentWillMount(){
        console.log('haii')
        firebase.database().ref('message').child(User.phone).child(this.state.person.phone)
            .on('child_added', (value) => {
                console.log("3333", value)
                this.setState((prevState) => {
                    return {
                        messageList: [...prevState.messageList, value.val()]
                    }
                })
                console.log("3333", this.state.messageList)
            })
        
            fetch('https://kuliahstem.prasetiyamulya.ac.id/web-api/newkuliah/')
            .then((response) => response.json())
            .then((data) => {
                  // TO DO filter data
                  let mappedData = data.map(item => {
                      return {
                          "day": item.day,
                          "section_name": item.section_name,
                          "faculty_name": item.faculty_name,
                          "location": item.location
                      }
                  });
                  this.setState({ data, mappedData }, () => {
                                        console.log("####", mappedData[0])

                  });

            })
            .catch((error) => console.error(error))
            .finally(() => {
                this.setState({ isLoading: false });
            });


        let yuhu = "hihi";

        this.setState(
            {steps: [
                {
                    id: '0',
                    message: 'Selamat Datang di SmartChat',
                    trigger: '1',
                  },
                  {
                    id: '1',
                    message: 'Ada Yang Bisa Kami Bantu?',
                    trigger: '2'
                  },
                  {
                      id: '2',
                      options: [
                        { value: 'filter', label: 'Filter Jadwal', trigger: '3' },
                        { value: 'seeAll', label: 'Lihat Semua Jadwal', trigger: '17' },
                      ],
                  },
                  {
                    id: '3',
                    message: 'Ingin Filter Jadwal Berdasarkan Apa?',
                    trigger: '4'
                  },
                  {
                    id: '4',
                    options: [
                      { value: 'filterHari', label: 'Hari', trigger: '5' },
                      { value: 'filterJudul', label: 'Judul', trigger: '6' },
                      { value: 'filterNama', label: 'Nama FM', trigger: '7' },
                      { value: 'filterRuang', label: 'Ruang Kelas', trigger: '8' },
                    ],
                },
                {
                    id: '5',
                    message: 'Silahkan Ketikan Nama Hari (Dalam Bahasa Inggris, diawali dengan huruf kapital)',
                    trigger: '9'
                  },
                  {
                    id: '6',
                    message: 'Silahkan Ketikan Judul Mata Kuliah (Tiap Awal Kata Kunci Silahkan Menggunakan Huruf Kapital)',
                    trigger: '10'
                  },
                  {
                    id: '7',
                    message: 'Silahkan Ketikan Nama FM (Tiap Awal Kata Kunci Silahkan Menggunakan Huruf Kapital)',
                    trigger: '11'
                  },
                  {
                    id: '8',
                    message: 'Silahkan Ketikan Ruang Kelas',
                    trigger: '12'
                  },
                  {
                    id: '9',
                    user: true,
                    trigger: '13'
                  },
                  {
                    id: '10',
                    user: true,
                    trigger: '14'
                  },
                  {
                    id: '11',
                    user: true,
                    trigger: '15'
                  },
                  {
                    id: '12',
                    user: true,
                    trigger: '16'
                  },
                  {
                    id: '13',
                    message: ({ previousValue, steps }) => {
                        var hasil = ""
                        for (let index = 0; index < this.state.mappedData.length; index++) {
                            if (this.state.mappedData[index]['day'].includes(previousValue)) {
                                hasil += this.state.mappedData[index]['section_name'] + '\n' + this.state.mappedData[index]['faculty_name'] + '\n' + this.state.mappedData[index]['day'] + '\n' + this.state.mappedData[index]['location'] + '\n' + '\n'
                            }
                        }
                        return hasil
                    },
                    trigger: '18'
                },
                {
                    id: '14',
                    message: ({ previousValue, steps }) => {
                        var hasil = ""
                        for (let index = 0; index < this.state.mappedData.length; index++) {
                            if (this.state.mappedData[index]['section_name'].includes(previousValue)) {
                                hasil += this.state.mappedData[index]['section_name'] + '\n' + this.state.mappedData[index]['faculty_name'] + '\n' + this.state.mappedData[index]['day'] + '\n' + this.state.mappedData[index]['location'] + '\n' + '\n'
                            }
                        }
                        return hasil
                    },
                    trigger: '18'
                },
                {
                    id: '15',
                    message: ({ previousValue, steps }) => {
                        var hasil = ""
                        for (let index = 0; index < this.state.mappedData.length; index++) {
                            if (this.state.mappedData[index]['faculty_name'].includes(previousValue)) {
                                hasil += this.state.mappedData[index]['section_name'] + '\n' + this.state.mappedData[index]['faculty_name'] + '\n' + this.state.mappedData[index]['day'] + '\n' + this.state.mappedData[index]['location'] + '\n' + '\n'
                            }
                        }
                        return hasil
                    },
                    trigger: '18'
                },
                {
                    id: '16',
                    message: ({ previousValue, steps }) => {
                        var hasil = ""
                        for (let index = 0; index < this.state.mappedData.length; index++) {
                            if (this.state.mappedData[index]['location'].includes(previousValue)) {
                                hasil += this.state.mappedData[index]['section_name'] + '\n' + this.state.mappedData[index]['faculty_name'] + '\n' + this.state.mappedData[index]['day'] + '\n' + this.state.mappedData[index]['location'] + '\n' + '\n'
                            }
                        }
                        return hasil
                    },
                    trigger: '18'
                },
                {
                    id: '17',
                    message: ({ previousValue, steps }) => {
                        var hasil = ""
                        for (let index = 0; index < this.state.mappedData.length; index++) {
                            
                            hasil += this.state.mappedData[index]['section_name'] + '\n' + this.state.mappedData[index]['faculty_name'] + '\n' + this.state.mappedData[index]['day'] + '\n' + this.state.mappedData[index]['location'] + '\n' + '\n'
                          
                        }
                        return hasil
                    },
                    trigger: '18'
                  },
                  {
                    id: '18',
                    message: 'Ada Yang Bisa Kami Bantu Lagi?',
                    trigger: '19'
                  },
                  {
                    id: '19',
                    options: [
                      { value: 'yesHelp', label: 'Ya, Bantu Lagi', trigger: '1' },
                      { value: 'noHelp', label: 'Tidak, Saya Sudah Selesai', trigger: '20' },
                    ],
                },
                {
                    id: '20',
                    message: 'Terima Kasih, Senang Bisa Membantu, Silahkan Kembali Ke Home Screen',
                    end: true,
                },
                
            ]}
        )
    }

    handleChange = key => val => {
        this.setState({[key]:val})
    }

    convertTime = (time) => {
        let d = new Date(time)
        let c = new Date();
        let result = (d.getHours()<10?'0':'')+d.getHours()+':';
        result += (d.getMinutes()<10?'0':''+d.getMinutes());
        if(c.getDay() !== d.getDay()){
            result = d.getDay()+' '+d.getMonth()+' '+result
        }
        return result
    }

    sendMessage = async () => {
        if (this.state.textMessage.length > 0){
            let msdgId = firebase.database().ref('message').child(User.phone).child(this.state.person.phone).push().key
            let updates = {}
            let message = {
                message: this.state.textMessage,
                time : firebase.database.ServerValue.TIMESTAMP,
                from: User.phone
            }
            updates['message/'+User.phone+'/'+this.state.person.phone+'/'+msdgId] = message;
            updates['message/'+this.state.person.phone+'/'+User.phone+'/'+msdgId] = message;
            firebase.database().ref().update(updates)
            this.setState({textMessage:' '})
        }
        // console.log(this.state.messageList)
    }

    renderRow = ({item}) => {
        return (
            <View style={{
                flexDirection: 'row',
                width: '60%',
                alignSelf: item.from === User.phone ? 'flex-end' : 'flex-start',
                backgroundColor: item.from === User.phone? '#21C004': '#FFFFFF',
                borderRadius: 5,
                marginBottom: 10,
            }}>
                <Text style={{color:'#000', padding: 12, fontSize: 15}}>
                    {item.message}
                </Text>
                <Text style={{color:'#B5CADD', padding: 3, fontSize: 12, alignSelf: 'flex-end', marginLeft: 100}}>{this.convertTime(item.time)}</Text>
            </View>
        )
    }

    render(){
        let{height, width} = Dimensions.get('window')
        return(
            <ChatBot steps={this.state.steps} botBubbleColor="#fff"
            botFontColor="#000" optionBubbleColor="#fff"
            optionFontColor="#000"
            />
        )
    }
}
