import React from 'react';
import {
    ActivityIndicator,
    AsyncStorage,
    StatusBar,
    StyleSheet,
    View,
} from 'react-native';
import firebase from 'firebase';
import User from '../User'

export default class AuthLoadingScreen extends React.Component {
    constructor(props) {
        super(props);
        this._bootsrapAsync();
    }

    componentWillMount(){
        const firebaseConfig = {
            apiKey: "AIzaSyAtkhH2djiErHSE0UOdocmD8AhoIurT5SQ",
            authDomain: "smart-chat-d0668.firebaseapp.com",
            projectId: "smart-chat-d0668",
            storageBucket: "smart-chat-d0668.appspot.com",
            messagingSenderId: "632526704470",
            appId: "1:632526704470:web:3242f9ea1e6f3061d890f7",
            measurementId: "G-48D60WZCQG"
          };
          // Initialize Firebase
          if (!firebase.apps.length) {
            firebase.initializeApp(firebaseConfig);
        }

    }

    _bootsrapAsync = async () => {
        User.phone = await AsyncStorage.getItem('userPhone');

        this.props.navigation.navigate(User.phone ? 'Bottom' : 'Auth');
    };

    render() {
        return (
            <View>
                <ActivityIndicator />
                <StatusBar barStyle="default" />
            </View>
        );
    }
}