import React from 'react'
import { SafeAreaView, Image, Text, TouchableOpacity, AsyncStorage, FlatList, View } from 'react-native'
import User from '../User'
import styles from '../constants/styles'
import {
    Container,
    Card,
    UserInfo,
    UserImgWrapper,
    UserImg,
    UserInfoText,
    UserName,
    PostTime,
    MessageText,
    TextSection,
} from '../constants/MessagesStyles'
import firebase from 'firebase'
import Icon from 'react-native-vector-icons/FontAwesome'

const editIcon = <Icon name="edit" size={30} color="#000" />
export default class HomeScreen extends React.Component {
    static navigationOptions = ({navigation}) => {
        return {
            title: 'Chats',
            headerTitleStyle: { alignSelf: 'center' },
            headerRight: (
                <TouchableOpacity onPress={() => navigation.navigate('Profile')} style={{marginRight: 10}}>
                    {editIcon}
                </TouchableOpacity>
            ),
            headerLeft: (
                <Text style={{marginLeft: 10}}>Edit</Text>
            )
        }
    }

    state = {
        users: [],
        data: [],
        isLoading: true
    }

    componentDidMount(){
        
        let dbRef = firebase.database().ref('users')
        dbRef.on('child_added', (val) => {
            let person = val.val();
            // console.log(person, User.phone)
            person.phone = val.key;
            if(person.phone == User.phone){
                User.name = person.name
            } else{
                this.setState((prevState) => {
                    return {
                        users: [...prevState.users, person]
                    }
                })
            }
            // console.log(this.state.users)
        })

    }
    



    render(){
        const { data, isLoading } = this.state;
        return(
            <SafeAreaView style = {styles.container}>
                 <FlatList 
                    data={this.state.users}
                    keyExtractor={(item)=>item.phone}
                    renderItem={({item}) => (
                        <Card onPress={() => this.props.navigation.navigate('Chat', item)}>
                        <UserInfo>
                            <UserImgWrapper>
                            <Image
                                style={{height: 50, width: 50}}
                                source={require('../img/logo.png')}
                            />
                            </UserImgWrapper>
                            <TextSection>
                            <UserInfoText>
                                <UserName>{item.name}</UserName>
                                
                            </UserInfoText>
                            </TextSection>
                        </UserInfo>
                        </Card>
                    )}
                />
                {/* <Text style={{fontSize: 20}}>Hi!, {this.state.users[0].name}</Text> */}
                <View style={{ flex: 1, padding: 24 }}>
        
          {/* <FlatList
            data={data}
            keyExtractor={({ id }, index) => id}
            renderItem={({ item }) => (
              <Text>{item.location}, {item.day}</Text>
            )}
          /> */}
      </View>
            </SafeAreaView>
        )
    }

}
