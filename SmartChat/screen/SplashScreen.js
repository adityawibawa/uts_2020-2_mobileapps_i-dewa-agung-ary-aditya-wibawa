import React, {Component} from 'react';
import {View, Text, Image, Dimensions, TouchableOpacity} from 'react-native';

const THREE_SECONDS = 3000;

export default class SplashScreen extends Component {
    static navigationOptions = {
        headerShown: false,
    };


    async componentDidMount(){
        setTimeout(() => {
            this.props.navigation.navigate('AuthLoading');
        }, THREE_SECONDS);
        console.log("njay")
    }


    render(){
        return (
        <View style={{flex: 1, alignItems: 'center', justifyContent: 'center', backgroundColor: '#E1E1E1'}}>
            <View style={{flex: 10, alignItems: 'center', justifyContent: 'center'}}>
                <Image
                    style={{height: 100, width: 100}}
                    source={require('../img/logo.png')}
                />
            </View>
        </View>
        );
    }
}