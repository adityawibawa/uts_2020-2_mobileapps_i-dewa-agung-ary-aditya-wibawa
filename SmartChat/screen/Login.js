import React, {Component} from 'react';
import {
    Platform, StyleSheet, Text,
    AsyncStorage, TouchableOpacity,
    TextInput, View, KeyboardAvoidingView
} from 'react-native';
import User from '../User'
import firebase from 'firebase'


export default class Login extends Component {
    static navigationOptions = {
        header: null
    }

    state = {
        phone: '',
        name: ''
    }

    componentWillMount() {
        AsyncStorage.getItem('userPhone').then(val => {
            if(val){
                this.setState({phone:val})
            }
        })
    }

    handleChange = key => val =>{
        this.setState({[key]:val})
    }

    submitForm = async () => {
        if (this.state.name == '' | this.state.phone == ''){
            alert(this.state.phone + '\n'+this.state.name)
        }else{
            await AsyncStorage.setItem('userPhone', this.state.phone)
            User.phone = this.state.phone
            firebase.database().ref('users/'+User.phone).set({name:this.state.name})

            this.props.navigation.navigate('Bottom')
            alert('phone successfully saved')
        }
    }

    render() {
        return (
            
            <View style={{flex: 1, alignItems: 'center', justifyContent: 'center', backgroundColor: '#E1E1E1'}}>
                <View style={{flex: 1, alignItems: 'center', justifyContent: 'center', maxWidth: 225, maxHeight: 500}}>
                    <TouchableOpacity style={{
                        alignSelf: 'flex-end',
                        marginRight: -60,
                        marginBottom: 50,
                    }} onPress={this.submitForm}>
                        <Text style={{fontWeight: 'bold', color: '#4299F2'}}>Next</Text>
                    </TouchableOpacity>
                    <Text style={{marginTop: 20, fontSize: 32, fontWeight: '100', color: '#000'}}>Your Identity</Text>
                    <Text style={{marginTop: 10, fontSize: 16, fontWeight: '100', color: '#000', textAlign: 'center'}}>Enter Your Phone Number and Name</Text>
                </View>
                <View style={{flex: 1, alignItems: 'center', justifyContent: 'flex-end', padding: 30, marginBottom: 50}}>
                <KeyboardAvoidingView style={{flex:1}}>
                <TextInput 
                        placeholder="Your Name"
                        style= {{
                          paddingBottom: 10,
                          paddingTop: 10,
                          paddingLeft: 125,
                          paddingRight: 125,
                          borderRadius: 5,
                          borderWidth: 1,
                          borderColor: '#ccc',
                          width: '90%',
                          marginBottom: 50,
                        }}
                        value = {this.state.name}
                        onChangeText = {this.handleChange('name')}
                    />
                <TextInput 
                        placeholder="ex: 082134522091"
                        style= {{
                          paddingBottom: 10,
                          paddingTop: 10,
                          paddingLeft: 100,
                          paddingRight: 100,
                          borderRadius: 5,
                          borderWidth: 1,
                          borderColor: '#ccc',
                          width: '90%',
                          marginBottom: 250,
                        }}
                        value = {this.state.phone}
                        onChangeText = {this.handleChange('phone')}
                    />
                </KeyboardAvoidingView>
                </View>
            </View>
            
        );
}
}