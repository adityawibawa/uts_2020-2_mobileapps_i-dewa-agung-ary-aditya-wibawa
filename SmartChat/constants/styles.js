import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
    },
    input: {
        padding: 5,
        borderWidth: 1,
        borderColor: '#ccc',
        width: '80%',
        margin: 8,
        borderRadius: 20,
    },
    btnText:{
        fontSize: 20,
        color: '#CC4F41',
    }
});

export default styles